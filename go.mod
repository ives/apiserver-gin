module apiserver-gin

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.4
	github.com/gin-gonic/gin v1.8.1
	github.com/go-playground/locales v0.14.0
	github.com/go-playground/universal-translator v0.18.0
	github.com/go-playground/validator/v10 v10.10.0
	github.com/go-redis/redis/v8 v8.7.0
	github.com/golang-jwt/jwt/v4 v4.2.0
	github.com/google/wire v0.5.0
	github.com/satori/go.uuid v1.2.0
	github.com/shopspring/decimal v1.3.1
	github.com/spf13/viper v1.12.0
	go.uber.org/zap v1.17.0
	golang.org/x/crypto v0.0.0-20220411220226-7b82a4e95df4
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gorm.io/driver/mysql v1.0.1
	gorm.io/gorm v1.20.1
)
